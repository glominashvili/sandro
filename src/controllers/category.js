/* eslint no-underscore-dangle: ["error", { "allow": ["_id", "_rev"] }] */
const HttpStatus = require('http-status-codes');
const ResponseHelper = require('../helpers/response');


/**
 * Category controller lists
 * all categories for partials
 * and templates
 */
class CategoryController {
  /**
   * Constructs controller
   * @param {object} templates Couchdb nano object
   * @param {object} partials Couchdb nano object
   * @param {object} config global config object
   * @param {object} logger global logger object
   */
  constructor(templates, partials, config, logger) {
    this.templates = templates;
    this.partials = partials;
    this.config = config;
    this.logger = logger;
  }

  /**
   * Unique filter of the array
   * @param {object} value current element
   * @param {integer} index current index
   * @param {object} self reference to array
   * @return {bool} if is unique
   */
  static onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
  }

  /**
   * null, undefined, '' filter
   * @param {object} elem reference to element
   * @return {bool} if is unique
   */
  static onlyNormal(elem) {
    return !!elem;
  }

  /**
   * List all distinct categories of partials
   * @param {object} req express request object
   * @param {object} res express response object
   */
  listPartialCategories(req, res) {
    const that = this;
    that.partials.find({
      limit: 1000000,
      selector: {},
      fields: ['category'],
    }, (listGetErr, listBody) => {
      if (listGetErr) {
        that.logger.error('Cant get list of partials');
        const response = ResponseHelper.create(false, HttpStatus.INTERNAL_SERVER_ERROR, 'CANT_GET_PARTIALS');
        res.status(response.code).send(response);
      } else {
        that.logger.info('Successfully got partials');
        const categories = listBody.docs
          .map(elem => elem.category)
          .filter(CategoryController.onlyUnique)
          .filter(CategoryController.onlyNormal);
        const response = ResponseHelper.create(true, HttpStatus.OK, 'SUCC_FOUND', categories);
        res.status(response.code).send(response);
      }
    });
  }

  /**
   * List all distinct categories of partials
   * @param {object} req express request object
   * @param {object} res express response object
   */
  listTemplateCategories(req, res) {
    const that = this;
    that.templates.find({
      limit: 1000000,
      selector: {},
      fields: ['category'],
    }, (listGetErr, listBody) => {
      if (listGetErr) {
        that.logger.error('Cant get list of template categories');
        const response = ResponseHelper.create(false, HttpStatus.INTERNAL_SERVER_ERROR, 'CANT_GET_TEMPLATES');
        res.status(response.code).send(response);
      } else {
        that.logger.info('Successfully got template categories');
        const categories = listBody.docs
          .map(elem => elem.category)
          .filter(CategoryController.onlyUnique)
          .filter(CategoryController.onlyNormal);
        const response = ResponseHelper.create(true, HttpStatus.OK, 'SUCC_FOUND', categories);
        res.status(response.code).send(response);
      }
    });
  }
}

module.exports = CategoryController;
