const HttpStatus = require('http-status-codes');

/**
 * Helper functions for response object
 * @namespace ResponseHelper
 */
const ResponseHelper = {
  /**
   * Creates success response
   * @param {string} dictionaryKey Key of a message in dictionary
   * @param {object} data Additional data to return
   * @return {object} Response Object
   */
  HtmlRenderingHelper(dictionaryKey, data = null) {
    return ResponseHelper.create(true, HttpStatus.OK, dictionaryKey, data);
  },

  /**
   * Creates success response
   * @param {bool} success If response was successfoul
   * @param {int} code Status Code
   * @param {string} dictionaryKey Key of a message in dictionary
   * @param {object} data Additional data to return
   * @return {object} Response Object
   */
  create(success, code, dictionaryKey, data = null) {
    return {
      success,
      code,
      msg: dictionaryKey,
      data,
    };
  },
};

module.exports = ResponseHelper;
