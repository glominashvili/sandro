const express = require('express');
const winston = require('winston');
const auth = require('basic-auth');
const bodyParser = require('body-parser');
const cors = require('cors');
const HttpStatus = require('http-status-codes');
const Nano = require('nano');
const Dictionary = require('./localization/dictionary');
const Migration = require('./src/migration');
const UserController = require('./src/controllers/user');
const TemplateController = require('./src/controllers/template');
const CategoryController = require('./src/controllers/category');
const ResponseHelper = require('./src/helpers/response');
const Config = require('./config.js');

const logger = winston.createLogger({
  level: Config.logger.level,
  transports: [
    new winston.transports.File({
      filename: Config.logger.file,
      format: winston.format.combine(
        winston.format.timestamp({
          format: 'YYYY-MM-DD HH:mm:ss',
        }),
        winston.format.json(),
      ),
      handleExceptions: true,
    }),
  ],
});

const nano = Nano({
  url: Config.db.url,
  log: (id, args) => {
    logger.debug(id, args);
  },
});
const users = nano.use('users');
const templates = nano.use('templates');
const partials = nano.use('partials');

/**
 * This function provdes json data
 * for unauthorized clients
 * @param {object} req Express request object
 * @return {object} Response object
 */
function getUnauthorizedResponse() {
  return ResponseHelper.create(false, HttpStatus.UNAUTHORIZED, 'UR_NOT_AUTHORIZED');
}

/**
 * This is custom authorizer function for
 * express basic auth extension
 * @param {object} req Exoress request
 * @param {object} res Exoress response
 * @param {function} next Callback
 */
function dbAuth(req, res, next) {
  const user = auth(req);
  if(user) {
    logger.info(`Trying to authorize in ${user.name}`);
    users.find({
      selector: {
        user: { $eq: user.name },
        password: { $eq: user.pass },
      },
      fields: ['user', 'role'],
      limit: 1,
    }, (err, body) => {
      if(body && body.docs.length > 0) {
        req.auth =  {
          user: body.docs[0].user,
          role: body.docs[0].role,
        };
        next();
      } else {
        logger.info(`Cant find user ${user.name}`);
        const response = ResponseHelper.create(false, HttpStatus.UNAUTHORIZED, '', 'CANT_AUTHORIZE');
        res.status(response.code).send(response);
      }
    });
  } else {
    logger.info('There is no credentials present in basic auth');
    const response = ResponseHelper.create(false, HttpStatus.UNAUTHORIZED, '', 'CANT_AUTHORIZE');
    res.status(response.code).send(response);
  }
}

/**
 * This is a factory to make filters
 * for user roles
 * @param {array} roles Alloweed roles 
 */
function roleFilterFactory(roles) {
  return (req, res, next) => {
    logger.info(`Trying to filter user role ${req.auth.user}:${req.auth.role}`);
    if (roles.indexOf(req.auth.role) !== -1) next();
    else {
      logger.info(`You are not welcome here ${req.auth.user}:${req.auth.role}`);
      const response = ResponseHelper.create(false, HttpStatus.BAD_REQUEST, '', 'NO_PERMISSION');
      res.status(response.code).send(response);
    }
  };
}
/**
 * This is callback for db init,
 * here we start web server.
 * */
function onDbInit() {
  const app = express();
  app.use(cors());
  app.use((req, res, next) => {
    if (req.method === 'GET' && req.path.indexOf('/dictionary') >= 0) {
      const response = ResponseHelper.create(true, HttpStatus.OK, '', Dictionary);
      res.status(response.code).send(response);
    } else next();
  });
  app.use(bodyParser.json({ limit: Config.server.maxRequestSize }));

  const userController = new UserController(users, Config, logger);
  const templateController = new TemplateController(templates, partials, Config, logger);
  const categoryController = new CategoryController(templates, partials, Config, logger);
  app.put('/users', dbAuth, roleFilterFactory(['admin']), (...args) => userController.add(...args));
  app.get('/users', dbAuth, roleFilterFactory(['admin']), (...args) => userController.list(...args));
  app.delete('/users/:id', dbAuth, roleFilterFactory(['admin']), (...args) => userController.remove(...args));
  app.get('/templates', dbAuth, roleFilterFactory(['admin', 'editor', 'viewer']), (...args) => templateController.list(...args));
  app.put('/templates', dbAuth, roleFilterFactory(['admin', 'editor']), (...args) => templateController.create(...args));
  app.post('/templates/:id', dbAuth, roleFilterFactory(['admin', 'editor']), (...args) => templateController.update(...args));
  app.get('/templates/:id', dbAuth, roleFilterFactory(['admin', 'editor', 'viewer']), (...args) => templateController.get(...args));
  app.delete('/templates/:id', dbAuth, roleFilterFactory(['admin']), (...args) => templateController.destroy(...args));
  if (Config.renderAuth) {
    app.post('/templates/:id/render/:fileType', dbAuth, roleFilterFactory(['admin', 'editor']), (...args) => templateController.render(...args));
  } else {
    app.post('/templates/:id/render/:fileType', (...args) => templateController.render(...args));
  }
  app.get('/partials', dbAuth, roleFilterFactory(['admin', 'editor', 'viewer']), (...args) => templateController.listPartials(...args));
  app.put('/partials', dbAuth, roleFilterFactory(['admin', 'editor']), (...args) => templateController.createPartial(...args));
  app.post('/partials/:id', dbAuth, roleFilterFactory(['admin', 'editor']), (...args) => templateController.updatePartial(...args));
  app.get('/partials/:id', dbAuth, roleFilterFactory(['admin', 'editor', 'viewer']), (...args) => templateController.getPartial(...args));
  app.delete('/partials/:id', dbAuth, roleFilterFactory(['admin', 'editor']), (...args) => templateController.destroyPartial(...args));
  app.get('/categories/templates', dbAuth, roleFilterFactory(['admin', 'editor', 'viewer']), (...args) => categoryController.listTemplateCategories(...args));
  app.get('/categories/partials', dbAuth, roleFilterFactory(['admin', 'editor', 'viewer']), (...args) => categoryController.listPartialCategories(...args));
  app.listen(Config.server.port);
}

const migration = new Migration(nano.db, Config, logger);
migration.migrate(onDbInit);
